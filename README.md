# Feb22

after three years the simple horizontal scrolling shoot em up is back.
Version 22 is half done in planing, and will finished ~~until Feburary~~ in 2022 [playable :]

#### One 10 months of fun
sure do a game in only one month is possible, but some time it works out other time it dont. Yeah this is the a second onE - next goal is do it in 10. It is a just for fun and nice to have project without any hurry. Learn about shoot em up games work, the history for good idears and programming some new better version of itself than before.
Fork the repository if your read until here, now !

ChangeLog
=========

* first ship and a rough animation
* first tileset with a Tiled created map
* new acting game logo
* take the good sprites from january and feb01


Roadmap
=======

- [x] ship movement
- [x] ship animation
- [ ] ship explosion
- [ ] ship collide tiles
- [ ] ship collide enemies / enemy bullets
- [ ] bullets
- [ ] bullet pool
- [ ] bullet collide
- [ ] bullet explosion
- [ ] enemies
- [ ] enemy bullets
- [ ] enemy bullet collide
- [ ] enemy explosion