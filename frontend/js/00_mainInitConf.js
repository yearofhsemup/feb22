const config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  pixelArt: true,
  backgroundColor: 0x000000,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: 640,
    height: 360
  },
  scene: [
    { key: 'preGame', active: true, init: init,  preload: gamePreload, create: gamePreCreate, update: gamePreUpdate },
    // { key: 'mainMenu', active: false, create: menuCreate, update: menuUpdate },
    { key: 'mainGame', active: false, create: gameCreate, update: gameUpdate },
    // {key: "testTile", active: true, preload: testPreload, create: testCreate, update: testUpdate},
    ],
  g: {
    fontStyle: { fontFamily: 'MODES', fontSize: 21, color: '#ffeefa' },
    state: {
      level: {seed:0},
      data: {
        allLevel: "",
        levelPos: 0,
      }
    }
  }
};

let game = new Phaser.Game(config);


function init() {
  //  Inject our CSS
  let element = document.createElement('style');
  document.head.appendChild(element);
  let sheet = element.sheet;
  let styles = '@font-face { font-family: "MODES"; src: url("assets/fonts/MODES.otf") format("opentype"); }\n';
  sheet.insertRule(styles, 0);
  // let lastHighScore = localStorage.getItem('highscore');
  // if(lastHighScore||false) {
  //   hud.values.highscore = parseInt(lastHighScore);
  // } else {
  //   localStorage.setItem('highscore',1000);
  // } 
}

function gamePreload() {

  let progress = this.add.graphics();
  let countFont = this.add.text(10,10,'',config.g.fontStyle);
  this.load.on('fileprogress', function (file, value) {
    if (file.key === 'gameLogo'){
      progress.clear();
      progress.fillStyle(0xffffff, 0.4);
      progress.fillRect(450, 500 - (value * 400), 200, value * 400);
      countFont.setText(value);
    }
  });

  this.load.on('complete', function () {
    progress.destroy();
    countFont.destroy();
  });

  this.load.script('webfont', 'lib/webfont.js');
  this.load.image('gameLogo', '/assets/images/feb_pixel_logo_22.png');
  // this.load.image('schalterBoard', '/assets/tiles/rockTilesFlags.png');
  // this.load.spritesheet('fg_tiles', '/assets/tiles/RockTilesFlags.png', { frameWidth: 32, frameHeight: 32 });
  // this.load.tilemapTiledJSON('foreground','assets/maps/Level_001.json');
  this.load.tilemapTiledJSON('back', 'assets/maps/Level_001.json');
  this.load.image('rockTilesFlags', 'assets/tiles/RockTilesFlags-extruded.png');

  this.load.spritesheet('ships','/assets/sprites/ships-spritesheets.png', { frameWidth: 48, frameHeight: 30, endFrame:30});

  // this.load.json('allLevelJson', '/get/unplayed/10');
}

let gameLogo;

function gamePreCreate() {
  gameLogo = this.add.image(config.width/2, config.height/2, 'gameLogo');
  gameLogo.setAlpha(0);

// pointerdown event fire over to next scene
// avoid that by poinerup

  this.input.on('pointerup', function (pointer) {
    stopStartScene(this, 'preGame', 'mainGame');
    }, this);

  let spaceKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
  spaceKey.on('down', (key, event) => {
    stopStartScene(this, 'preGame', 'mainGame');
  });

  // just an early random seed
  
  config.g.state.level.seed = Math.floor(Math.random()*10000000);
  config.g.state.data.allLevel = this.cache.json.get('allLevelJson');
  config.g.state.data.levelPos = 0;
}

function gamePreUpdate() {
  let toGo = gameLogo.alpha;
  if(toGo>=1) {
    stopStartScene(this, 'preGame', 'mainGame');
  } else {
    gameLogo.setAlpha(toGo+.005);
  }
}

const stopStartScene = (that, stopName, startName) => {
  that.scene.stop(stopName);
  setTimeout( () => { that.scene.start(startName);}, 150);
};