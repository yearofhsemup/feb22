let ship = {o:{},p:10,s:5};
let gameStatic = {ship:{speed:0.05}};
let keysInput = {};
function gameCreate() {
	console.log("create Game here!!!");
	map = this.make.tilemap({ key: 'back', tileWidth: 32, tileHeight: 32});
	// https://github.com/sporadic-labs/tile-extruder
	// tileset texture bleeding solution
  let tileset = map.addTilesetImage('RockTilesFlags','rockTilesFlags',32, 32, 1, 2);
  let layer = map.createDynamicLayer(0, tileset, 0, 30);
  // let staticMapLayer = map.convertLayerToStatic(layer);
  // layer.setActive(false);
  // staticMapLayer.setCollisionBetween(1, 60);

  keysInput = this.input.keyboard.addKeys('LEFT,RIGHT,UP,DOWN,A,D,W,S,X,SPACE,SHIFT,L');

  // let cursors = this.input.keyboard.createCursorKeys();
  // let controlConfig = {
  //     camera: this.cameras.main,
  //     left: cursors.left,
  //     right: cursors.right,
  //     speed: 0.1
  // };

  // controls = new Phaser.Cameras.Controls.FixedKeyControl(controlConfig);

  // this.cameras.main.setBounds(0, 0, layer.x + layer.width + 600, 0);


  const conf_shipUp = {
  	key: 'shipUp',
  	frames: this.anims.generateFrameNumbers('ships', { frames: [0,1,2] }),
  	frameRate: 15,
  	repeat: 0,
  };

	const conf_shipUpDown = {
  	key: 'shipUpDown',
  	frames: this.anims.generateFrameNumbers('ships', { frames: [2,1,0] }),
  	frameRate: 15,
  	repeat: 0,
  };

  const conf_shipDown = {
  	key: 'shipDown',
  	frames: this.anims.generateFrameNumbers('ships', { frames: [0,3,4] }),
  	frameRate: 15,
  	repeat: 0,
  };

  const conf_shipDownUp = {
  	key: 'shipDownUp',
  	frames: this.anims.generateFrameNumbers('ships', { frames: [4,3,0] }),
  	frameRate: 15,
  	repeat: 0,
  };

  this.anims.create(conf_shipUp);
  this.anims.create(conf_shipUpDown);
  this.anims.create(conf_shipDown);
  this.anims.create(conf_shipDownUp);

  ship.o = this.add.sprite(200,200, 'ships');
  this.cameras.main.startFollow(ship.o, false, 0.2, 0.2);
}


function gameUpdate(time, delta) {
	// controls.update(delta);
	if(keysInput.D.isDown || keysInput.RIGHT.isDown) {
		ship.o.x += gameStatic.ship.speed*delta;

	} else if(keysInput.A.isDown || keysInput.LEFT.isDown) {
		ship.o.x -= gameStatic.ship.speed*delta;
	}
	if(keysInput.W.isDown || keysInput.UP.isDown) {
		ship.o.y -= gameStatic.ship.speed*delta;
		if(ship.o.anims.getName()!='shipUp') {
			ship.o.anims.play('shipUp');
		}
	} else if(keysInput.S.isDown || keysInput.DOWN.isDown) {
		ship.o.y += gameStatic.ship.speed*delta;
		if(ship.o.anims.getName()!='shipDown') {
			ship.o.anims.play('shipDown');
		}
	} else if(ship.o.anims.getName()==='shipUp') {
		if(!ship.o.anims.isPlaying) ship.o.anims.play('shipUpDown');
	} else if(ship.o.anims.getName()==='shipDown') {
		if(!ship.o.anims.isPlaying) ship.o.anims.play('shipDownUp');
	}
	
	ship.o.x += (gameStatic.ship.speed*1.05)*delta;
}


// function testPreload() {

// }

// function testCreate() {

// }

// function testUpdate(time, delta) {
// 	controls.update(delta);
// }